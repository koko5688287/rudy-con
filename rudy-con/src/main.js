// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import globalApi from '@/assets/globalApi.js'
// import './assets/css/main.css'
Vue.config.productionTip = false;
Vue.use(globalApi);

var tolink = '';
router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.requiresAuth)) {
    if (window.localStorage.getItem('username') == undefined || window.localStorage.getItem('username') == null) {
      next({
        path: '/',
      })
      tolink = to.path;
    } else {
      if(window.localStorage.getItem('version') == undefined || window.localStorage.getItem('version') == null || window.localStorage.getItem('version') != '0.0.3') {
          window.localStorage.clear();
          next({
            path: '/',
          });
      } else {
        if(tolink != '') {
          var direction = tolink;
          tolink = '';
          next({ path: direction})
        } else {
          next()
        } 
      }
    }
  } else {
    next()
  }
  // next();
})

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
})


