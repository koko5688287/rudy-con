import Vue from 'vue'
import Router from 'vue-router'
import Login from '@/components/Login'
import AppPassword from '@/components/AppPassword'
import Menu from '@/components/Menu'
import CreditList from '@/components/CreditList'
import TransactionList from '@/components/TransactionList'
import TransactionDetail from '@/components/TransactionDetail'
import PaymentSelection from '@/components/PaymentSelection'
import PaymentDetail from '@/components/PaymentDetail'
import JVLogin from '@/components/JVLogin'
Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Login',
      component: Login
    },
    {
      path: '/apppass',
      name: 'AppPassword',
      component: AppPassword,
      meta: {
        requiresAuth: true
      }
    },
    {
      path: '/menu',
      name: 'Menu',
      component: Menu,
      meta: {
        requiresAuth: true
      }
    },
    {
      path: '/creditslist',
      name: 'CreditList',
      component: CreditList,
      meta: {
        requiresAuth: true
      }
    },
    {
      path: '/transactionlist',
      name: 'TransactionList',
      component: TransactionList,
      meta: {
        requiresAuth: true
      }
    },
    {
      //path: '/transactiondetail/:id?/:step?',
      path: '/transactiondetail/:id?',
      name: 'TransactionDetail',
      component: TransactionDetail,
      meta: {
        requiresAuth: true
      }
    },
    {
      path: '/paymentselect',
      name: 'PaymentSelection',
      component: PaymentSelection,
      meta: {
        requiresAuth: true
      }
    },
    {
      path: '/paymentdetail',
      name: 'PaymentDetail',
      component: PaymentDetail,
      meta: {
        requiresAuth: true
      }
    },
    {
      path: '/jvlogin',
      name: 'JVLogin',
      component: JVLogin,
      meta: {
        requiresAuth: true
      }
    }
  ]
})
