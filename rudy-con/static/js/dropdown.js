function DropDown(el) {
	this.dd = el;
	this.initEvents();
}
DropDown.prototype = {
    val : "",
    // onChange: function(d){},
	initEvents : function() {
		var obj = this;
		obj.val = obj.dd.find(".search-content-input-dropdown-options-item").eq(0).attr("value");
		// obj.dd.find(".con-dropdown-header").html(obj.dd.find(".con-dropdown-options-items").eq(0).html());
        obj.dd.find(".search-content-input-dropdown-options-item").on('click', function(event){
		    obj.val = $(this).attr("value");
		    // obj.dd.find(".con-dropdown-header").html($(this).html());
		});	
		obj.dd.off('click');
		obj.dd.on('click', function(event){
		    $(".search-content-input-dropdown").not($(this)[0]).removeClass('active');
			$(this).toggleClass('active');
			event.stopPropagation();
		});	
		$(document).click(function() {
    		// all dropdowns
    		obj.dd.removeClass('active');
    	});
	},
	// setOnChange : function(fn){
	// 	if(typeof(fn) === 'function' || fn instanceof Function){
	// 		this.onChange = fn;
	// 	}
	// }
}