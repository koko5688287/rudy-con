function loaderOn(){
    $('#loader').addClass('active');
    $('#app').addClass('page-loading-blur');
}

function loaderOff(){
    $('#loader').removeClass('active');
    $('#app').removeClass('page-loading-blur');
}

function splashScreenOn(){
    $('#splashScreen').addClass('active');
}

function splashScreenOff(){
    setTimeout(()=>{
        $('#splashScreen').removeClass('active');
    },500);
}

/*------------------------------ Start API -----------------------------------*/
var CON_API_URL = 'https://api.merudy.com/appeyes/merudy/MTIzcXdlMTIz6969/th/';
function CON_API(action,url_params,form_params,onSuccess,onError,xhr_progress_listener){
    loaderOn();
	$.ajax({ 
	    xhr: function() {
			var xhr = new window.XMLHttpRequest();
			xhr.upload.addEventListener("progress", xhr_progress_listener, false);
			return xhr;
		},
		type: 'POST', 
		url: CON_API_URL+action+'/'+((url_params)?('?'+url_params):''), 
		data: form_params, 
		dataType: 'json',
		success : function( data) {
		    loaderOff();
			if(onSuccess){
			    onSuccess(data);
			}
			else{
				console.log(data);
			}
		},
		error 	: function( data) {
		    loaderOff();
		    console.log(data);
			if(onError){
			    onError(false);
			}
		}
	});
}
/*------------------------------- End API ------------------------------------*/

/*----------------------------- Start utils ----------------------------------*/
function validatePhone(phone) {
  var re = /^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/;
  return re.test(phone);
}
function validateEmail(email) {
  var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(email);
}
function getBase64(file,callback) {
    loaderOn();
    var reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = function () {
        if(callback){
            loaderOff();
            callback(reader.result);
        }
    };
    reader.onerror = function (error) {
        console.log('Error: ', error);
    };
}
function onNumberKeyPress(elementQuery,keyCode,isWithoutDot){
    if(keyCode >= 48 && keyCode <= 57){
        return true;
    }
    else if(keyCode == 46 && !isWithoutDot){
        var oldval = $(elementQuery).val();
        $(elementQuery).val(oldval+'.0');
        if($(elementQuery).val() == ""){
            $(elementQuery).val(oldval);
            return false;
        }
        else{
            $(elementQuery).val(oldval);
            return true;
        }
    }
    else{
        return false;
    }
}
function getTimestamp(){
    var timeStampInMs = 
        window.performance && 
        window.performance.now && 
        window.performance.timing && 
        window.performance.timing.navigationStart ? 
        window.performance.now() + window.performance.timing.navigationStart : Date.now();
    return timeStampInMs;
}
function isNullVal(val){
    return (!val || val == "" || val == "null" || val == null);
}
function getUrlVars() {
    var vars = {};
    var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi,    
    function(m,key,value) {
      vars[key] = value;
    });
    return vars;
}
/*------------------------------ End utils -----------------------------------*/
